package com.dasmitian.unopointscounter;

import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainPresenter {

    private MainView mainView;
    private MainModel mainModel;
    private SharedPreferencesClass sharedPreferences;

    public MainPresenter(MainView mainView){
        this.mainView = mainView;
        mainModel = new MainModel();
        sharedPreferences = new SharedPreferencesClass(mainView.getApplicationContext());
    }

    public void addPlayer(String name) {
        mainModel.addPlayer(name);
    }

    public ArrayList<Player> getPlayers() {
        return mainModel.getPlayers();
    }

    public void newPlayer() {
        if(mainModel.getPlayersCount() < 10) {
            mainView.showNewPlayerDialog();
        } else {
            mainView.showMessage("Player limit reached");
        }
    }

    public void addPoints(int points, int position) {
        mainModel.addPoints(points, position);
        if(mainModel.getPlayerPoints(position) >= 1000){
            mainView.finishGame(mainModel.getPlayerName(position));
        }
    }

    public void subPoints(int points, int position) {
        mainModel.subPoints(points, position);
    }

    public void resetPoints() {
        mainModel.resetAllPoints();
    }

    public void resetGame() {
        mainModel.resetPlayers();
        sharedPreferences.clearPlayers();
    }

    public void saveGame(){
        sharedPreferences.savePlayers(mainModel.getPlayers());
    }

    public void loadPlayers(){
        mainModel.setPlayers(sharedPreferences.getPlayers());
    }

    public void clearPlayers() {
        sharedPreferences.clearPlayers();
    }
}
