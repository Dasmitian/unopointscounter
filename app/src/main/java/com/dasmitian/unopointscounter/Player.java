package com.dasmitian.unopointscounter;

public class Player {

    int points = 0;
    String name;

    public Player(String name){
        this.name = name;
    }

    public void addPoints(int points){
        this.points += points;
    }

    public void subtractPoints(int points){
        this.points -= points;
    }

    public void resetPoints(){
        this.points = 0;
    }

    public String getName(){
        return this.name;
    }

    public String getPoints(){
        return String.valueOf(this.points);
    }

}

