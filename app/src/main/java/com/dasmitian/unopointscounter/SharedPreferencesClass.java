package com.dasmitian.unopointscounter;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

public class SharedPreferencesClass {

    private SharedPreferences preferences;
    SharedPreferences.Editor prefsEditor;

    public SharedPreferencesClass(Context context){
        this.preferences = context.getSharedPreferences("players", Context.MODE_PRIVATE);

    }

    public void savePlayers(ArrayList<Player> players){
        prefsEditor = preferences.edit();
        int size = players.size();
        prefsEditor.putInt("players_count", size);
        for(int i = 0; i < size; i++){
            prefsEditor.putString("player_" + i + "_name", players.get(i).getName());
            prefsEditor.putInt("player_" + i + "_points", Integer.parseInt(players.get(i).getPoints()));
        }
        prefsEditor.apply();
    }

    public ArrayList<Player> getPlayers(){
        int size = preferences.getInt("players_count", 0);
        ArrayList<Player> players = new ArrayList<>();
        for(int i = 0; i < size; i++){
            Player player = new Player(preferences.getString("player_" + i + "_name", null));
            player.addPoints(preferences.getInt("player_" + i + "_points", 0));
            players.add(player);
        }
        return players;
    }

    public void clearPlayers(){
        prefsEditor = preferences.edit();
        prefsEditor.clear();
        prefsEditor.apply();
    }




}
