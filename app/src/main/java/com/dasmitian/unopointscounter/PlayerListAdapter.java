package com.dasmitian.unopointscounter;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PlayerListAdapter extends BaseAdapter implements ListAdapter {

    private ArrayList<Player> players;
    private Context context;
    private Interface anInterface;

    public PlayerListAdapter(ArrayList<Player> players, Context context, Interface anInterface){
        this.players = players;
        this.context = context;
        this.anInterface = anInterface;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int i) {
        return players.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.player, null);

            TextView tvName = view.findViewById(R.id.tv_player_name);
            final TextView tvPoints = view.findViewById(R.id.tv_player_points);
            Button btnAddPoints = view.findViewById(R.id.btn_add_points);
            Button btnSubPoints = view.findViewById(R.id.btn_sub_points);

            tvName.setText(players.get(position).getName());
            tvPoints.setText(players.get(position).getPoints());

            btnAddPoints.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPointsDialog("add", position);

                }
            });

            btnSubPoints.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    anInterface.showPointsDialog("sub", position);
                }
            });

        }
        return view;
    }
}
