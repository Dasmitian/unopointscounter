package com.dasmitian.unopointscounter;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainView extends AppCompatActivity implements Interface{

    private MainPresenter presenter;
    private ListView playersList;
    private PlayerListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_main);
        presenter = new MainPresenter(this);

        Button btnAddPlayer = findViewById(R.id.btn_add_player);
        btnAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.newPlayer();
            }
        });

        Button btnResetPoints = findViewById(R.id.btn_reset_points);
        btnResetPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPointsAlertDialog();
            }
        });

        Button btnResetGame = findViewById(R.id.btn_reset_game);
        btnResetGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGameAlertDialog();
            }
        });
        presenter.loadPlayers();
        playersList = findViewById(R.id.lv_players);
        createPlayersList();

    }

    private void resetGameAlertDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to reset game?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.resetGame();
                        createPlayersList();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create();
        builder.show();
    }

    private void resetPointsAlertDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to reset all points?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.resetPoints();
                        createPlayersList();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create();
        builder.show();
    }

    public void showNewPlayerDialog(){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.new_player, null);

        final EditText etName = dialogView.findViewById(R.id.et_new_player_name);

        Button btnDone = dialogView.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addPlayer(String.valueOf(etName.getText()));
                createPlayersList();
                alertDialog.dismiss();
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    public void showPointsDialog(final String operation, final int position){
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.points, null);

        final EditText etPoints = dialogView.findViewById(R.id.et_new_points);

        Button btnDone = dialogView.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(operation.equals("add") && !String.valueOf(etPoints.getText()).isEmpty()){
                    presenter.addPoints(Integer.parseInt(String.valueOf(etPoints.getText())), position);
                }
                if(operation.equals("sub") && !String.valueOf(etPoints.getText()).isEmpty()){
                    presenter.subPoints(Integer.parseInt(String.valueOf(etPoints.getText())), position);
                }
                createPlayersList();
                alertDialog.dismiss();
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    private void createPlayersList(){
        adapter = new PlayerListAdapter(presenter.getPlayers(), getApplicationContext(), this);
        playersList.setAdapter(adapter);
    }


    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void finishGame(String playerName) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Player " + playerName + " lost the game!")
                .setPositiveButton("Play again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.clearPlayers();
                        createPlayersList();
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        builder.create();
        builder.show();
    }

    public void onPause(){
        super.onPause();

        presenter.saveGame();
    }
}