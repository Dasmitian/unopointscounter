package com.dasmitian.unopointscounter;

import java.util.ArrayList;
import java.util.List;

public class MainModel {
    private ArrayList<Player> players;
    private int playersCount = 0;

    public MainModel(){
        players = new ArrayList<>();
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void addPlayer(String name) {
        players.add(new Player(name));
        playersCount++;
    }

    public int getPlayersCount(){
        return playersCount;
    }

    public void addPoints(int points, int position) {
        players.get(position).addPoints(points);
    }

    public void subPoints(int points, int position) {
        players.get(position).subtractPoints(points);
    }

    public void resetAllPoints(){
        for(Player player: players){
            player.resetPoints();
        }
    }

    public void resetPlayers(){
        players.clear();
    }

    public int getPlayerPoints(int position){
        return Integer.parseInt(players.get(position).getPoints());
    }

    public String getPlayerName(int position){
        return players.get(position).getName();
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
}
